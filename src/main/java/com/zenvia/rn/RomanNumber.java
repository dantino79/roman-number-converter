package com.zenvia.rn;

import java.util.Arrays;
import java.util.List;

public class RomanNumber {

    // wiki: https://bitbucket.org/hansbrattberg/scala-katas/src/faf82c88d358/src/romannumerals.scala
    private List<String> ones = Arrays.asList("", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX");
    private List<String> tens = Arrays.asList("", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC");
    private List<String> hundreds = Arrays.asList("", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM");
    private List<String> thousands = Arrays.asList("", "M", "MM", "MMM");

    public String converter(Integer number){
        String romanNumber = "";

        romanNumber += hasThousands(number);
        romanNumber += hasHundreds(number);
         romanNumber += hasTens(number);
        romanNumber += hasOnes(number);

        System.out.println(romanNumber+"-"+number);

        return romanNumber;
    }

    private String hasThousands(Integer number){
        String numberParse = number.toString();
        if(numberParse.length() < 4)
            return "";
        String dddd = numberParse.substring(0, 1);
        return thousands.get(Integer.parseInt(dddd));
    }

    private String hasHundreds(Integer number){
        String numberParse = number.toString();
        String ddd;

        if(numberParse.length() < 3)
            return "";
        else if(numberParse.length() == 3){
            ddd = numberParse.substring(0, 1);
            return hundreds.get(Integer.parseInt(ddd));
        }
        else{
            ddd = numberParse.substring(1, 2);
            return hundreds.get(Integer.parseInt(ddd));
        }
    }

    private String hasTens(Integer number){
        String numberParse = number.toString();
        String dd;

        if(numberParse.length() < 2)
            return "";
        else if(numberParse.length() == 2){
            dd = numberParse.substring(0, 1);
            return tens.get(Integer.parseInt(dd));
        }
        else if(numberParse.length() == 3){
            dd = numberParse.substring(1, 2);
            return tens.get(Integer.parseInt(dd));
        }
        else{
            dd = numberParse.substring(2, 3);
            return tens.get(Integer.parseInt(dd));
        }
    }

    private String hasOnes(Integer number){
        String numberParse = number.toString();
        String d;

        if(numberParse.length() == 1)
            return ones.get(number);
        else if(numberParse.length() == 2){
            d = numberParse.substring(1, 2);
            return ones.get(Integer.parseInt(d));
        }
        else if(numberParse.length() == 3){
            d = numberParse.substring(2, 3);
            return ones.get(Integer.parseInt(d));
        }
        else{
            d = numberParse.substring(3, 4);
            return tens.get(Integer.parseInt(d));
        }
    }
}
