package com.zenvia.test;

import static org.junit.Assert.assertEquals;

import com.zenvia.rn.RomanNumber;
import org.junit.Test;

public class RomanNumberTest {

    @Test
    public void converterTest(){

        RomanNumber romanNumber = new RomanNumber();

        // ones
        assertEquals("I", romanNumber.converter(1));
        assertEquals("II", romanNumber.converter(2));
        assertEquals("III", romanNumber.converter(3));
        assertEquals("IV", romanNumber.converter(4));
        assertEquals("V", romanNumber.converter(5));
        assertEquals("VI", romanNumber.converter(6));
        assertEquals("VIII", romanNumber.converter(8));
        assertEquals("IX", romanNumber.converter(9));

        //tens
        assertEquals("X", romanNumber.converter(10));
        assertEquals("XI", romanNumber.converter(11));
        assertEquals("XV", romanNumber.converter(15));
        assertEquals("XIX", romanNumber.converter(19));

        assertEquals("XX", romanNumber.converter(20));
        assertEquals("XXII", romanNumber.converter(22));
        assertEquals("XXV", romanNumber.converter(25));
        assertEquals("XXIX", romanNumber.converter(29));

        assertEquals("XL", romanNumber.converter(40));
        assertEquals("XLIV", romanNumber.converter(44));
        assertEquals("XLV", romanNumber.converter(45));
        assertEquals("XLIX", romanNumber.converter(49));

        assertEquals("L", romanNumber.converter(50));
        assertEquals("LV", romanNumber.converter(55));
        assertEquals("LIX", romanNumber.converter(59));

        assertEquals("XC", romanNumber.converter(90));
        assertEquals("XCV", romanNumber.converter(95));
        assertEquals("XCIX", romanNumber.converter(99));

        assertEquals("CX", romanNumber.converter(110));
        assertEquals("DCL", romanNumber.converter(650));
        assertEquals("CMX", romanNumber.converter(910));


        assertEquals("MCLX", romanNumber.converter(1160));
        assertEquals("MM", romanNumber.converter(2000));
    }
}
